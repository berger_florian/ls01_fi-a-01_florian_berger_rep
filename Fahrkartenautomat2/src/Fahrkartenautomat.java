import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double anzahlFahrkarten; //ich habe mich hier f�r einen double entschieden damit dieser in der methode fahrkartenbestellungErfassen verwendet werden kann
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen("Ticketpreis in �: ");
       while(zuZahlenderBetrag < 0) {
    	   System.out.println("Du h�lst dich wohl f�r ganz lustig DU VOGEL probiers mal von vorne!");
       		zuZahlenderBetrag = fahrkartenbestellungErfassen("Ticketpreis in �: ");
       }
       anzahlFahrkarten = fahrkartenbestellungErfassen("Anzahl der Fahrkarten (max. 10): ");
       if(anzahlFahrkarten > 10 || anzahlFahrkarten < 1) {
       		System.out.println("Du h�lst dich wohl f�r ganz lustig DU VOGEL, jetz kriegste nur eine Fahrkarte, und hoffentlich benimmste dich beim n�chsten mal!");
       		anzahlFahrkarten = 1;
       }
       
       zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten; // Hier wird der zuZahlenderBetrag * die anzahlFahrkarten gerechnet um so die zu bezahlende summe zu berechnen
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %1.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
    	   if(eingeworfeneM�nze > 2)
    		   System.out.println("Diese Zahlungsm�glichkeit wird nicht akzeptiert!");
    	   else 
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = fahrkartenBezahlung(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f� %n", r�ckgabebetrag);
    	   rueckgeldAusgeben(r�ckgabebetrag);
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    public static double fahrkartenbestellungErfassen(String text) {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print(text);
    	double zahl = tastatur.nextDouble();
    	return zahl;
    }
    public static double fahrkartenBezahlung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double ergebnis;
    	ergebnis = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return ergebnis;
    }
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)	 
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
    }
        public static void rueckgeldAusgeben(double r�ckgabebetrag) {
        	System.out.println("wird in folgenden M�nzen ausgezahlt:");
     	   
            while(r�ckgabebetrag >= 1.99) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 1.99;
            }
            while(r�ckgabebetrag >= 0.99) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 0.99;
            }
            while(r�ckgabebetrag >= 0.49) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.49;
            }
            while(r�ckgabebetrag >= 0.19) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.19;
            }
            while(r�ckgabebetrag >= 0.09) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.09;
            }
            while(r�ckgabebetrag >= 0.04)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.04;
            }
        }
}
//Aufgabe 3.1
//Variable "zuZahlenderBetrag", 			Datentyp "double"
//Variable "eingezahlter Gesamtbetrag", 	Datentyp "double"
//Variable "eingeworfeneM�nze", 			Datentyp "double"
//Variable "r�ckgabebetrag", 				Datentyp "double"
//Variable "i",								Datentyp "integer"
//Aufgabe 3.2
//			Line 22:  zuZahlenderBetrag - eingezahlterGesamtbetrag
//			Line 25:  eingezahlterGesamtbetrag += eingeworfeneM�nze
//			Line 45:  r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag