import java.util.ArrayList;

/**
 * Diese Klasse modelliert ein Raumschiff
 * 
 * @author Florian Berger
 * @version 4.16.0 von 06.2020
 */

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>(); // nach new wird ein Konstruktor aufgerufen
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	
	/**
	 * Parameterlose Konstruktor der Klasse Raumschiff
	 */
	Raumschiff() {
	}
	
	/**
	 * Konstruktor f�r die Klasse Raumschiff
	 * @param photonentorpedoAnzahl				Anzahl der Torpedos
	 * @param energieversorgungInProzent		Energieversorgung des Schiffs in Prozent
	 * @param schildeInProzent					Schilde des Schiffs in Prozent
	 * @param huelleInProzent					H�lle des Schiffs in Prozent
	 * @param lebenserhaltungssystemeInProzent	Lebenserhaltungssysteme in Prozent
	 * @param androidenAnzahl					Anzahl der Androiden
	 * @param schiffsname						Name des Schiffs
	 */
	Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
			int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	/**
	 * F�gt eine Ladung dem ladungsverzeichnis hinzu
	 * @param neueLadung		Objekt der Klasse Ladung 
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Diese Methode �berpr�ft ob Torpedos vorhanden sind, 
	 * falls ja wird eine "Abgeschossen" und der Parameter(photonentorpedoAnzahl) um 1 reduziert, 
	 * falls nicht wird dies deutlich gemacht
	 * @param r		Dieser Parameter bezieht sich auf das Raumschiff welches abgeschossen wird
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl--;
			nachrichtenAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		} else {
			nachrichtenAnAlle("-=*Click*=-");
		}
	}
	
	/**
	 * Diese Methode �berpr�ft ob genug Energy vorhanden ist, 
	 * falls ja wird die Kanone abgefeuert und der  Energieversorgung(Parameter(energievesorgungInProzent)) um 50 verringert, 
	 * falls es nicht m�glich ist wird dies deutlich gemacht
	 * @param r		Dieser Parameter bezieht sich auf das Raumschiff welches abgeschossen wird
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent > 50) {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtenAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		} else {
			nachrichtenAnAlle("-=*Click*=-");
		}
	}
	
	/**
	 * Diese Methode wird ausgef�hrt durch die Methoden phaserkanoneSchiessen und photonentorpedoSchiessen.
	 * Dadurch wird beim Ziel-Raumschiff das Schild(Parameter(schildeInProzent)) um 50 reduziert und die Methode treffervermerken ausgef�hrt
	 * danach wird �berpr�ft ob die Schilde(Parameter(schildeInProzent)) 0 oder weniger sind, wenn nicht passiert nichts
	 * ansonsten wird die H�lle(Parameter(huelleInProzent)) und die Energieversorgung(Parameter(energieversorgungInProzent) um 50 reduziert
	 * daraufhin wird �berpr�ft ob die H�lle bei 0 oder weniger ist, falls die zutrifft werden die Lebenserhaltungssysteme(Parameter(lebenserhaltungssystemeInProzent) auf 0 gesetzt,
	 * falls nicht dann passiert nichts weiter
	 * @param r		Dieser Parameter bezieht sich auf das Raumschiff welches abgeschossen wird
	 */
	private void treffer(Raumschiff r) {
		r.setSchildeInProzent(r.getSchildeInProzent() - 50);
		r.treffervermerken();
		if (r.getSchildeInProzent() <= 0) {
			r.setHuelleInProzent(r.getHuelleInProzent() - 50);
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
			if (r.getHuelleInProzent() <= 0) {
				r.setLebenserhaltungssystemeInProzent(0);
				nachrichtenAnAlle("Lebenserhaltungssysteme wurden vernichtet");
			}
		}
	}
	/**
	 * Diese Methode wird ausgef�hrt durch die Methode treffer.
	 * und gibt einen Text aus der sich auf das angegeben Raumschiff bezieht: "X wurde getroffen!"
	 */
	public void treffervermerken() {
		System.out.println(schiffsname + " wurde getroffen!");
	}
	
	/**
	 * Sobald diese Methode aufgerufen wird kann ein String angegeben werden welche dann in den Broadcast aufgenommen wird
	 * @param message 			Ist ein String der immer in den Array vom broadcastKommunikator eingetragen wird
	 */
	public void nachrichtenAnAlle(String message) {
		broadcastKommunikator.add("\n" + this.schiffsname + ": " + message);
	}
	
	/**
	 * Alle Strings die durch die Methode nachrichtAnAlle hinzugef�gt wurden werden ausgegeben
	 * @return Es werden alle Eintr�ge die in den Array eingetragen wurden ausgegeben
	 */
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}
	
	/**
	 * Noch nicht verwendete Methode der Expertenaufgaben
	 * @param anzahlTorpedos		Mit diesem Parameter wird die anzahl der Torpedos �bernommen und ver�ndert
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
	}

	/**
	 * Noch nicht verwendete Methode der Expertenaufgaben
	 * @param schutzschilde		Schutzschilde werden repariert
	 * @param energieversorgung	Energieversorgung wird repariert
	 * @param schiffshuelle		Schiffsh�lle wird repariert
	 * @param anzahlDroiden		Anzahl der Droiden die zur Reparatur eingesetzt werden
	 */
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
	}

	/**
	 * In dieser Methode werden werden alle Informationen zu einem Raumschiff
	 * (schiffsname, photonentorpedoAnzahl, energieversorgungInProzent, schildeInProzent, huelleInProzent, lebenserhaltungssystemeInProzent, androidenAnzahl)
	 * abgerufen und dann wiedergegeben um herauszufinden wie der Zustand des Schiffes ist
	 */
	public void zustandRaumschiff() {
		System.out.println("Schiff:" + schiffsname);
		System.out.println("Anzahl der Photonentorpedos: " + photonentorpedoAnzahl);
		System.out.println("Energieversorgung: " + energieversorgungInProzent + " %");
		System.out.println("Schilde: " + schildeInProzent + " %");
		System.out.println("H�lle: " + huelleInProzent + " %");
		System.out.println("Lebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent + " %");
		System.out.println("Anzahl der Androiden: " + androidenAnzahl);
	}
	
	/**
	 * In dieser Methode wird eine Schleife erstellt welche zuerst �berpr�ft wie gro� das ladungsverzeichnis ist.
	 * Abh�ngig von der gr��e wird bestimmt wie oft die for-schleife durchgeht
	 * und mit jedem durchgang wird der name des Gegenstandes und die menge des Gegenstandes aus dem ladungsverzeichnis �bernommen
	 * und dies wird dann ausgegeben 
	 * z.B.: Forschungssonde: 5
	 */
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.print(ladungsverzeichnis.get(i).getBezeichnung() + ": ");
			System.out.println(ladungsverzeichnis.get(i).getMenge());
		}
	}
	
	/**
	 * Noch nicht verwendete Methode der Expertenaufgaben
	 */
	public void ladungsverzeichnisAufraeumen() {
	}
}
