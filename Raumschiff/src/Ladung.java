
	/**
	 * Diese Klasse modelliert eine Ladung
	 * 
	 * @author Florian Berger
	 * @version 4.16.0 von 06.2020
	 */
public class Ladung {
	private String bezeichnung;
	private int menge;
	Ladung(){
		
	}
	
	/**
	 * Konstruktor f�r die Klasse Ladung
	 * @param bezeichnung		Bezeichnung des Gegenstands
	 * @param menge				Menge des Gegenstands
	 */
	Ladung(String bezeichnung, int menge){
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
}