
public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Ladung ferengiSchneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonenSchwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(ferengiSchneckensaft);
		klingonen.addLadung(klingonenSchwert);
		romulaner.addLadung(borgSchrott);
		romulaner.addLadung(roteMaterie);
		romulaner.addLadung(plasmaWaffe);
		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(photonentorpedo);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		System.out.println();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reperaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());
	}

}
