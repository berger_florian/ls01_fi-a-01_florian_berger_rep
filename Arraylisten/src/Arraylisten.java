import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Arraylisten {
	
	public static void ausgabe(ArrayList<Integer> zahl) {
		for (int j = 0; j < zahl.size(); j++) {
			System.out.printf("myList[%2d] : %d\n", j, zahl.get(j));
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random r = new Random();
		ArrayList<Integer> zahl = new ArrayList<Integer>();
		
		for (int i = 0; i<20; i++) {
			zahl.add(1 + r.nextInt(9));
		}
		
		ausgabe(zahl);
		Scanner s = new Scanner(System.in);
		System.out.println("Welche Zahl soll gesucht werden: ");
		int suchen = s.nextInt();
		int z�hlen = 0;
		for(int k = 0; k<zahl.size();k++) {
			if(zahl.get(k) == suchen) {
				z�hlen++;
				System.out.printf("myList[%2d] : %d\n", k, zahl.get(k));
				zahl.remove(k);
				k--;
			}
		}
		System.out.printf("Die Zahl %d gibt es %d mal\n", suchen, z�hlen);
		
		System.out.printf("Liste nach L�schen von %d", suchen);
		ausgabe(zahl);
		System.out.println("Nach 0 hinzuf�gen: ");
		for(int k = 0; k<zahl.size();k++) {
			if(zahl.get(k) == 5) {
				zahl.add(k+1, 0);
			}
		}
		ausgabe(zahl);
		}
	}

