import java.util.Scanner;
public class Summe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int summeBis = 0;
		int summe = 0;
		int zaehler = 0;
		
		System.out.println("Bitte geben Sie eine Zahl ein, bis welche addiert werden soll!");
		summeBis = sc.nextInt();
		
		sc.close();
		
		//Kopfgesteuerte Schleife
		while(zaehler <= summeBis) {
			summe = summe + zaehler;
			zaehler += 1;
		}
		System.out.println(summe);
		
		summe = 0;
		zaehler = 0;
		
		//Fu�gesteuerte Schleife
		do {
			summe = summe + zaehler;
			zaehler = zaehler + 1;
			
		}while (zaehler <= summeBis);
		
		System.out.println(summe);
		
		summe = 0;
		
		for(int i = 1; i <= summeBis; i++) {
			summe = summe + i;
		}
		System.out.println(summe);
		}
}

