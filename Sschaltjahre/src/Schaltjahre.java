import java.util.Scanner;

public class Schaltjahre {

	public static void main(String[] args) {
		
		int jahr;
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Jahreszahl ein!");
		jahr = tastatur.nextInt();
		tastatur.close();
		
		if(jahr % 4 != 0) 
			keins();
			else if(jahr <= 1582) 
				eins();
		    else 
					if(jahr % 400 == 0) 
						eins();
					else if(jahr % 100 == 0) 
						keins();
					else 
						eins();
			
		}
		
	public static void keins() {
		System.out.println("Dieses Jahr ist kein Schaltjahr");
	}
	public static void eins() {
		System.out.println("Dieses Jahr ist ein Schaltjahr");
	}
}

