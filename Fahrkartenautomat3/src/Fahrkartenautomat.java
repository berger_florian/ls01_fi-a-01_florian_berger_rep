import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double anzahlFahrkarten; //ich habe mich hier f�r einen double entschieden damit dieser in der methode fahrkartenbestellungErfassen verwendet werden kann
       int ausschalten = 0;
       
       while(ausschalten != 111) {
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       while(zuZahlenderBetrag == 0) {
       		zuZahlenderBetrag = fahrkartenbestellungErfassen();
       }
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %1.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
    	   if(eingeworfeneM�nze > 2) //diese if schleife �berpr�ft ob das eingeworfene geld nicht die 2 euro m�nze �bersteigt
    		   System.out.println("Diese Zahlungsm�glichkeit wird nicht akzeptiert!");
    	   else 
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = fahrkartenBezahlung(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       if(r�ckgabebetrag > 0.0) //diese if schleife �berpr�ft ob der noch zu zahlende betrag bereits abbezahlt ist oder noch vorhanden 
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f� %n", r�ckgabebetrag);
    	   rueckgeldAusgeben(r�ckgabebetrag);
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.\n");
       ausschalten = tastatur.nextInt();
       }      
    }
    public static double fahrkartenbestellungErfassen() {
    	String fahrkarten[] = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	double zahl[] = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	//das string array enth�lt alle fahrkarten und der double array deren preise
    	for (int i = 0; i<fahrkarten.length; i++) {
        	System.out.printf("%-2d %-35s %-5.2f %s\n", i+1, fahrkarten[i], zahl[i], "�"); //hier werden die fahrkarten und ihre preise ausgegeben
        }
    	System.out.println("\nIhre Bestellung: ");
    	Scanner tastatur = new Scanner(System.in);
    	double preis = 0;
    	while (preis == 0) {
    	int i = tastatur.nextInt();
    	if(i > fahrkarten.length || i < 1) // hier wird �berpr�ft ob die angegebene nummer der fahrkarte auch tats�chlich existiert
    		System.out.println(">>Falsche Eingabe<<\nIhre Bestellung: ");
    		else
    			preis = zahl[i-1];
    	}
    	int menge = 0;
        System.out.println("Anzahl der Fahrkarten max. 10: ");
    	menge = tastatur.nextInt();
    	if(menge > 10 || menge < 1) {
        System.out.println("Du h�lst dich wohl f�r ganz lustig DU VOGEL, jetz kriegste nur eine Fahrkarte, und hoffentlich benimmste dich beim n�chsten mal!");
    	menge = 1; //wenn mehr als 10 oder weniger als 1 eingeben werden wird die anzahl automatisch auf eine fahrkarte gestellt
    	}
    	preis = preis * menge;
    	return preis;
    }		
    public static double fahrkartenBezahlung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double ergebnis;
    	ergebnis = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return ergebnis;
    }
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)	 
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
    }
        public static void rueckgeldAusgeben(double r�ckgabebetrag) {
        	System.out.println("wird in folgenden M�nzen ausgezahlt:");
     	   
            while(r�ckgabebetrag >= 1.99) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 1.99;
            }
            while(r�ckgabebetrag >= 0.99) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 0.99;
            }
            while(r�ckgabebetrag >= 0.49) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.49;
            }
            while(r�ckgabebetrag >= 0.19) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.19;
            }
            while(r�ckgabebetrag >= 0.09) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.09;
            }
            while(r�ckgabebetrag >= 0.04)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.04;
            }
        }
}